# GMD Crash Course

game for GMD course - Forspil
I recommend opening this file in Raw format, for better readability.


Link to the youtube demostration: https://youtu.be/v9J_IRUGzLc
Link to playable webGl build on UnityPlay: https://play.unity.com/mg/other/webgl-hhm


List of Sources (I do not own the rights over the following resourses. There were used in educational purpose) :

Brackeys Youtube channel :
            - THIRD PERSON MOVEMENT in Unity https://www.youtube.com/watch?v=4HpC--2iowE&t=933s&ab_channel=Brackeys
            - MAKING YOUR FIRST LEVEL in Unity with ProBuilder! https://www.youtube.com/watch?v=YtzIXCKr8Wo&t=725s&ab_channel=Brackeys
            - Introduction to AUDIO in Unity https://www.youtube.com/watch?v=6OT43pvUyfY&ab_channel=Brackeys
Sunny Valley Studio Youtube channel:
            - Unity 3d Third Person Movement with Cinemachine P1- P4 https://www.youtube.com/watch?v=IJdidB83aig&t=469s&ab_channel=SunnyValleyStudio
gamesplusjames Youtube channel:
            - How To Make a Rhythm Game 1-4 https://www.youtube.com/watch?v=cZzf1FQQFA0&ab_channel=gamesplusjames
Audio - Dadi Freyr (link to spotify page: https://open.spotify.com/artist/3Hb64DQZIhDCgyHKrzBXOL?si=sqjh2cDWSFW7kJ51fTO8ug) 

Unity Packages:
    - Arcade Machines Pack 01-02 -LowPoly Pack by AurynSky
    - Simple Gems Ultimate Animated - AurynSky
    - Basic Motion FREE - Kevin Iglesias 
    - Dance Animations FREE - Kevin Iglesias
    - Dialogue Editor - Runia Dev
    - Flexible Cel Shader - Zachary Petersen
    - Food Monsters Character and Animation - Catgear Games
    - SpotLight and Structure - SpaceZeta


Many thanks for Graham Tattersall and Yu Chao for their awesome articles on how to built a rhythm game. Can find them here: https://gamasutra.com/blogs/GrahamTattersall/20190515/342454/Coding_to_the_Beat__Under_the_Hood_of_a_Rhythm_Game_in_Unity.php



Now, in regards to game:
Controls: WASD and mouse for normal character movement.
Interact with an NPC : E
For Rhythm game: 
     Blue -> A
     Red -> S
     Yellow -> K
     Green -> L

Known bugs:
    - By pressing exit in the pause menu (Esc) in the MainScene you cannot "Start" again the game from the MainMenu scene
    - Movement of the camera is still pressent in pause mode
    - Option menu cannot be displayed in the MainScene from the Pause Menu
    - Pausing the game doesn't pause the dialog
    - The movement of the character is not smooth due to bad math when calculating the camera rotation when following the character.


I guess, that's all! Have fun playing!
