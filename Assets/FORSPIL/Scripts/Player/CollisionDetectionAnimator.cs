﻿using UnityEngine;
using UnityEngine.Serialization;
using Vector3 = UnityEngine.Vector3;

namespace MyAssets.Scripts
{
    public class CollisionDetectionAnimator : MonoBehaviour
    {
        public Animator animator;
        public CharacterController controller;
        public LayerMask layerMask;
        public Transform eyes;
        public Transform feet;


        void Update()
        {
            DetectCollissions();
        }

        private void DetectCollissions()
        {
            Collider[] hitCollisionsEyes = Physics.OverlapSphere(eyes.position, 0.5f, layerMask);

            foreach (var collider in hitCollisionsEyes)
            {
                switch (collider.tag)
                {
                    case "Climb":
                    {
                        if (Input.GetButtonDown("Climb") && controller.isGrounded)
                        {
                            animator.SetTrigger("Climb");
                            Vector3 offset = new Vector3(0f, 0f, 0.2f);
                            Vector3 position = controller.transform.position + controller.transform.eulerAngles + offset;
                            // Controller.SimpleMove(new Vector3(10f, 0f, 10f));
                        }

                        break;
                    }
                    case "Door":
                    {
                        if (Input.GetButtonDown("OpenDoor") && controller.isGrounded)
                        {
                            animator.SetTrigger("OpenDoor");
                        }

                        break;
                    }
                }
            }


            Collider[] hitCollidersFeet = Physics.OverlapSphere(feet.position, 0.5f, layerMask);
            foreach (var collider in hitCollidersFeet)
            {
                switch (collider.tag)
                {
                    case "Climb":
                    {
                        if (Input.GetButtonDown("ClimbDown") && controller.transform.position.y > 0f)
                        {
                            animator.SetTrigger("ClimbDown");
                        }

                        break;
                    }
                }
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Stairs")
            {
                Debug.Log("Stairs!!");
                controller.stepOffset = 1f;
            }
        }


        private void OnTriggerExit(Collider other)
        {
            if (other.tag == "Stairs")
            {
                Debug.Log("Stairs!! - not!");

                controller.stepOffset = .3f;
            }
        }
    }
}