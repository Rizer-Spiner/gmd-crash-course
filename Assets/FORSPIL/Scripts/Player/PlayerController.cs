﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MyAssets.Scripts
{
    public class PlayerController : MonoBehaviour
    {
        public CharacterController controller;
        public Camera Camera;
        public Animator animator;

        public float movementSpeed;
        public float desiredRotation;
        public float rotationSpeed;
        public float gravity;

        private bool _jumping;

        private bool canMove = true;
  

        private static readonly int Move = Animator.StringToHash("move");
        private static readonly int FramesWithoutMove = Animator.StringToHash("framesWithoutMove");
        private static readonly int Jumps = Animator.StringToHash("Jumps");

        private void OnEnable()
        {
            DialogScript.dialog += StopMovement;
            DialogScript.finisheDialog += ResumeMovement;
        }

        private void OnDisable()
        {
            DialogScript.dialog -= StopMovement;
            DialogScript.finisheDialog -= ResumeMovement;
        }

        private void ResumeMovement()
        {
            canMove = true;
        }

        private void StopMovement(string name)
        {
            canMove = false;
        }
        
       
        void Update()
        {
            if (canMove)
            {
                if (controller.isGrounded)
                {
                    gravity = -9.81f;
                    animator.ResetTrigger(Jumps);
                    if (Input.GetButton("Jump"))
                    {
                        SetCorrectAnimation("jump");
                        animator.SetFloat(Move, 0f);
                    }
                    else
                    {
                        float x = Input.GetAxisRaw("Horizontal");
                        float z = Input.GetAxisRaw("Vertical");

                        Vector3 movement = new Vector3(x, 0f, z).normalized;
                        Vector3 rotatedMovement = Quaternion.Euler(0f, Camera.transform.rotation.eulerAngles.y, 0f) *
                                                  movement;
                        Vector3 vecticalMovement = Vector3.up * gravity;

                        controller.Move(vecticalMovement + rotatedMovement * (movementSpeed * Time.deltaTime));

                        if (rotatedMovement.magnitude > 0)
                        {
                            desiredRotation = Mathf.Atan2(rotatedMovement.x, rotatedMovement.z) * Mathf.Rad2Deg;
                        }

                        Quaternion currentRotation = transform.rotation;
                        Quaternion targetRotation = Quaternion.Euler(0f, desiredRotation, 0f);
                        transform.rotation = Quaternion.Lerp(currentRotation, targetRotation,
                            rotationSpeed * Time.deltaTime);

                        if (rotatedMovement != Vector3.zero)
                        {
                            animator.SetInteger(FramesWithoutMove, 0);
                            SetCorrectAnimation("walking");
                        }
                        else
                        {
                            animator.SetFloat(Move, 0);
                            var framesWithoutMove = animator.GetInteger(FramesWithoutMove);
                            framesWithoutMove += 1;
                            animator.SetInteger(FramesWithoutMove, framesWithoutMove);
                        }
                    }
                }
            }
        }

        private void SetCorrectAnimation(string instruction)
        {
            switch (instruction)
            {
                case "walking":
                {
                    float currentAnimationSpeed = animator.GetFloat("move");

                    if (currentAnimationSpeed < 0.2)
                    {
                        currentAnimationSpeed += Time.deltaTime * 2;
                        currentAnimationSpeed = Mathf.Clamp(currentAnimationSpeed, 0, 0.2f);
                    }
                    else
                    {
                        if (currentAnimationSpeed > 1)
                        {
                            currentAnimationSpeed += Time.deltaTime * 2;
                        }
                        else
                        {
                            currentAnimationSpeed = 1;
                        }
                    }

                    animator.SetFloat("move", currentAnimationSpeed);
                    break;
                }
                case "jump":
                {
                    animator.SetTrigger("Jumps");
                    break;
                }
            }
        }
    }
}