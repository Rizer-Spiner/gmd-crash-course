﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Serialization;

public class PlayerMovementScript : MonoBehaviour
{
    public CharacterController Controller;
    public Animator animator;
    public float gravity;

    public float rotationSpeed;
    public float movementSpeed;
    public float rotationVelocity;
    public float rotationTime = 0.1f;
    
    
    private static readonly int Jumps = Animator.StringToHash("Jumps");
    private static readonly int FramesWithoutMove = Animator.StringToHash("framesWithoutMove");
    private static readonly int Move = Animator.StringToHash("move");


    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        animator.ResetTrigger(Jumps);
        if (Input.GetButton("Jump"))
        {
           
            SetCorrectAnimation("jump");
        }
        else
        {
            Vector3 direction = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));

            if (Controller.isGrounded)
            {
                if (direction != Vector3.zero)
                {
                    animator.SetInteger(FramesWithoutMove, 0);
                    RotatePlayer(direction);
                    SetCorrectAnimation("walking");
                    Controller.Move(direction * (movementSpeed * Time.deltaTime));
                }
                else
                {
                    animator.SetFloat(Move, 0);
                    var framesWithoutMove = animator.GetInteger(FramesWithoutMove);
                    framesWithoutMove += 1;
                    animator.SetInteger(FramesWithoutMove, framesWithoutMove);
                }
            }
        }
    }

    private void RotatePlayer(Vector3 direction)
    {
        float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
        float smooth = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref rotationVelocity, rotationTime);
        transform.rotation = Quaternion.Euler(0f, smooth, 0f);
    }


    private void SetCorrectAnimation(String instruction)
    {
        switch (instruction)
        {
            case "walking":
            {
                float currentAnimationSpeed = animator.GetFloat("move");

                if (currentAnimationSpeed < 0.2)
                {
                    currentAnimationSpeed += Time.deltaTime * 2;
                    currentAnimationSpeed = Mathf.Clamp(currentAnimationSpeed, 0, 0.2f);
                }
                else
                {
                    if (currentAnimationSpeed > 1)
                    {
                        currentAnimationSpeed += Time.deltaTime * 2;
                    }
                    else
                    {
                        currentAnimationSpeed = 1;
                    }
                }

                animator.SetFloat("move", currentAnimationSpeed);
                break;
            }
            case "jump":
            {
                animator.SetTrigger("Jumps");
                break;
            }
        }
    }
}