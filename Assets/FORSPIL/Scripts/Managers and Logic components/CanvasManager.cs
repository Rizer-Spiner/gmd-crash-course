﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyAssets.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CanvasManager : MonoBehaviour
{
    public Canvas[] _canvasArray;
    public Dictionary<String, Canvas> _canvasDictionary;
    public String startCanvas;
    public bool stopGameTime;

    private void OnEnable()
    {
        _canvasDictionary = new Dictionary<string, Canvas>();
        foreach (var c in _canvasArray)
        {
            c.enabled = false;
            _canvasDictionary.Add(c.name, c);
        }
        showCanvas(startCanvas);
        
    }

    private void OnDisable()
    {
        DialogScript.dialog -= Pause;
    }

    void Update()
    {
        if (Input.GetKey("escape") && !SceneManager.GetActiveScene().name.Equals("MainMenu"))
        {
            stopGameTime = true;
            showCanvas("GameMenu");
            
        }
    }

    public void showCanvas(String name)
    {
        if (stopGameTime && name.Equals("TalkMenu") )
        {
            Pause(name);
        }
        Canvas[] flattenList = _canvasDictionary.Values.ToArray();
        foreach (Canvas c in flattenList)
        {
            c.enabled = false;
        }

        if (name != null && !name.Equals("") && _canvasDictionary[name] != null)
        {
            _canvasDictionary[name].enabled = true;
        }
        else
        {
            Resume();
        }
    }

    private void Resume()
    {
        Time.timeScale = 1f;
        stopGameTime = false;
    }

    private void Pause(string name)
    {
        Time.timeScale = 0f;
    }
}