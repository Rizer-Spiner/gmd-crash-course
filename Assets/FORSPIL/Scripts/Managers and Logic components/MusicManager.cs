﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour
{
    public static MusicManager instance;

    public AudioClip[] playlist;
    public AudioSource Source;
    private int index;

    private void OnEnable()
    {
        DontDestroyOnLoad(this);
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        Source.clip = playlist[0];
        index++;
        Source.Play();
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene().name.Equals("Rhythm2D"))
        {
            Source.Stop();
        }
        else if (!Source.isPlaying)
        {
            if (index < playlist.Length)
            {
                index = 0;
            }

            index++;
            Source.clip = playlist[index];
            Source.Play();
        }
    }
}