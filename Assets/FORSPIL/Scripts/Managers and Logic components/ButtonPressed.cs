﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPressed : MonoBehaviour
{
    public CanvasManager Manager;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("escape"))
        {
            Manager.showCanvas("GameMenu");
        }
    }
}
