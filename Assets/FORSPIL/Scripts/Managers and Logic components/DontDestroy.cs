﻿using System;
using UnityEngine;

namespace MyAssets.Scripts
{
    public class DontDestroy : MonoBehaviour
    {
        private void OnEnable()
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}