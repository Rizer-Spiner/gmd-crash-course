﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class GameManager : MonoBehaviour
{
    public Color[] Colors;
    public bool soundEffectsEnabled;
    public bool lightsEnabled;

    public static GameManager instance;

    private void OnEnable()
    {
        DontDestroyOnLoad(this);
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    public Color[] getColors()
    {
        return Colors;
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}