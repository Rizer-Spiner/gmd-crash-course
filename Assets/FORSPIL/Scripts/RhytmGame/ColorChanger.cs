﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChanger: MonoBehaviour
{
    private SpriteRenderer _spriteRenderer;

    void Start()
    {
        _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    public void changeColor()
    {
        _spriteRenderer.color = Color.red;
    }
}
