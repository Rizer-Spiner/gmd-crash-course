﻿using System.Collections;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;

namespace FORSPIL.Scripts.RhytmGame
{
    public class SongManager : MonoBehaviour
    {

        public AudioSource theMusic;
        public float bpm;
        public float offsetOfFirstBeat;
        public bool startPlaying;
 
        public Track[] noteTracks; 
        public Text scoreText;
        public Text multiplier;

        public static SongManager instance;

        public int curretScore;
        public int combo;
        public int scorePerNote = 100;
        public int scorePerGoodNote = 125;
        public int scorePerPerfectNote = 150;
    


        private void OnEnable()
        {
            Note.goodHit += GoodHit;
            Note.normalHit += NormalHit;
            Note.perfectHit += PerfectHit;
            Note.missHit += NoteMissed;
        }

        private void OnDisable()
        {
            Note.goodHit -= GoodHit;
            Note.normalHit -= NormalHit;
            Note.perfectHit -= PerfectHit;
            Note.missHit -= NoteMissed;
        }

        void Start()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
                return;
            }
            combo = 1;
            scoreText.text = "Score: " + curretScore;
            multiplier.text = "Multiplier: x" + combo;
        
            AudioListener.pause = true;
        }

        void Update()
        {
            if (!startPlaying)
            {
                if (Input.anyKeyDown)
                {
                    AudioListener.pause = false;
                    startPlaying = true;
                    foreach (var track in noteTracks)
                    {
                        track.start = true;
                    }
                    theMusic.Play();
                }
            }

            if (startPlaying && !theMusic.isPlaying)
            {
                StartCoroutine(EndGame());
            }
            
        }

        IEnumerator EndGame()
        {
            yield return new WaitForSeconds(2.5f);
            SceneLoadManager.instance.LoadScene("MainMenu");
        }

        private void NoteHit()
        {
            scoreText.text = "Score: " + curretScore;
            multiplier.text = "Multiplier: x" + combo;
        }

        public void NormalHit()
        {
            combo++;
            curretScore += scorePerNote * combo;
            NoteHit();
        }

        public void GoodHit()
        {
            combo++;
            curretScore += scorePerGoodNote * combo;
            NoteHit();
        }

        public void PerfectHit()
        {
            combo++;
            curretScore += scorePerPerfectNote * combo;
            NoteHit();
        }
        public void NoteMissed()
        {
            combo = 0;
            multiplier.text = "Multiplier: x" + combo;
        }
    
    
    }
}
