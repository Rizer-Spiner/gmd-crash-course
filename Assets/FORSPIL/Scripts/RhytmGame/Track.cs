﻿using System;
using FORSPIL.Scripts.RhytmGame;
using UnityEngine;

namespace DefaultNamespace
{
    public class Track : MonoBehaviour
    {
        public bool start;
        public SongManager _gameManager;
        [NonSerialized] public float songBpm;
        [NonSerialized] public float secPerBeat;
        public float songPosition;

        public float songPositionInBeats;
        [NonSerialized] public float dspSongTime;
        [NonSerialized] public float firstBeatOffset;

        public Transform NotesActivator;
        public KeyCode NotesKey;
        public GameObject noteObject;

        public float[] notes;
        [NonSerialized] public int nextIndex;

        private void Start()
        {
            songBpm = _gameManager.bpm;
            secPerBeat = 60f / songBpm;
            dspSongTime = (float) AudioSettings.dspTime;
            firstBeatOffset = _gameManager.offsetOfFirstBeat;
            start = false;
        }

        private void Update()
        {
            if (start)
            {
                songPosition = (float) (AudioSettings.dspTime - dspSongTime - firstBeatOffset);
                songPositionInBeats = songPosition / secPerBeat;

                if (nextIndex < notes.Length && notes[nextIndex] < songPositionInBeats + 3)
                {
                    Instantiate(noteObject, gameObject.transform, false);
                    nextIndex++;
                }
            }
        }
    }
}