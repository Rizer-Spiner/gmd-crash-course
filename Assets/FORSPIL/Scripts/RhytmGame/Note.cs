﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DefaultNamespace
{
    public class Note : MonoBehaviour

    {
        //statics
        public float songBpm;
        public float dspSongTime;
        public float secPerBeat;
        public float firstBeatOffset;


        //change over time
        public float songPosition;
        public float songPositionInBeats;

        //needed to calculate properly the movement
        public float beatOfThisNote;
        public float initialSongPosInBeats;

        //movement
        public float timeElapsed;
        public float lerpDuration;
        public Transform activator;
        private Vector3 SpawnPos;
        private Vector3 RemovePos;

        //Note specific
        private bool canBePressed;
        private bool obtained;
        public KeyCode keyToPress;
        public GameObject hitEffect, goodEffect, perfectEffect, missEffect;

        public delegate void NormalHitEvent();

        public static event NormalHitEvent normalHit;

        public delegate void GoodHitEvent();

        public static event GoodHitEvent goodHit;

        public delegate void PerfectHitEvent();

        public static event PerfectHitEvent perfectHit;

        public delegate void MissHitEvent();

        public static event MissHitEvent missHit;

        void Start()
        {
            songBpm = transform.parent.GetComponentInParent<Track>().songBpm;
            dspSongTime = transform.parent.GetComponentInParent<Track>().dspSongTime;
            secPerBeat = transform.parent.GetComponentInParent<Track>().secPerBeat;
            firstBeatOffset = transform.parent.GetComponentInParent<Track>().firstBeatOffset;

            StartCoroutine(UpdateSongTime());
            activator = transform.parent.GetComponentInParent<Track>().NotesActivator;
            keyToPress = transform.parent.GetComponentInParent<Track>().NotesKey;

            SpawnPos = new Vector3(activator.position.x, activator.position.y + 10f, activator.position.z);
            RemovePos = new Vector3(activator.position.x, activator.position.y, activator.position.z);
            transform.localPosition = SpawnPos;

            int index = transform.parent.GetComponentInParent<Track>().nextIndex;
            beatOfThisNote = transform.parent.GetComponentInParent<Track>().notes[index - 1];
            initialSongPosInBeats = songPositionInBeats;
            lerpDuration = beatOfThisNote - initialSongPosInBeats;
        }

        void Update()
        {
            if (timeElapsed < lerpDuration)
            {
                transform.position = Vector2.Lerp(
                    SpawnPos,
                    RemovePos,
                    timeElapsed / lerpDuration
                );

                timeElapsed = songPositionInBeats - initialSongPosInBeats;
            }
            else
            {
                transform.position -= Vector3.up * (Time.deltaTime * 3);
            }

            if (Input.GetKeyDown(keyToPress))
            {
                if (canBePressed)
                {
                    StartCoroutine(ObtainNote());
                }
            }
        }


        IEnumerator ChangeColor()
        {
            gameObject.GetComponent<SpriteRenderer>().color = Color.red;
            yield return null;
        }

        IEnumerator UpdateSongTime()
        {
            while (true)
            {
                songPosition = (float) (AudioSettings.dspTime - dspSongTime - firstBeatOffset);
                songPositionInBeats = songPosition / secPerBeat;
                yield return null;
            }
        }

        IEnumerator ObtainNote()
        {
            if (Math.Abs(transform.position.y) > .25f)
            {
                normalHit?.Invoke();
                InstantiateEffect(hitEffect);
            }
            else if (Math.Abs(transform.position.y) > .05f)
            {
                goodHit?.Invoke();
                InstantiateEffect(goodEffect);
            }
            else
            {
                perfectHit?.Invoke();
                InstantiateEffect(perfectEffect);
            }


            obtained = true;
            Destroy(gameObject);
            yield return null;
        }


        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Activator"))
            {
                canBePressed = true;
            }
            else if (other.CompareTag("ButtomLine"))
            {
                Destroy(gameObject);
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag("Activator"))
            {
                canBePressed = false;
                if (!obtained)
                {
                    missHit?.Invoke();
                    StartCoroutine(ChangeColor());
                    InstantiateEffect(missEffect);
                   
                }
            }
        }

        private void InstantiateEffect(GameObject effect)
        {
            float spawnY = Random.Range
            (Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).y,
                Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height)).y);
            float spawnX = Random.Range
            (Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).x,
                Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0)).x);


            Vector3 position = new Vector3(Random.Range(spawnX * 0.35f, spawnX),
                Random.Range(spawnY * 0.35f, spawnY), 0f);

            Quaternion rotation = new Quaternion(effect.transform.rotation.x, effect.transform.rotation.y,
                effect.transform.rotation.z * Random.Range(0f, 90f), effect.transform.rotation.w);
            Instantiate(effect, position, rotation);
        }
    }
}