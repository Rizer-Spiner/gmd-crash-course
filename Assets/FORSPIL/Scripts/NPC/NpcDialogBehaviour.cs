﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MyAssets.Scripts
{
    public class NpcDialogBehaviour : MonoBehaviour
    {
        public Animator animator;
        public float defaultTime;
        private float _randomStartTime;
        public float timeRemaining;
        public String trigger;

 
        void Start()
        {
            timeRemaining = defaultTime;
            _randomStartTime = Random.Range(10, 70);
            defaultTime = _randomStartTime;
        }

   
        void Update()
        {
            if (TimeOut())
            {
                timeRemaining = defaultTime;
                animator.SetTrigger(trigger);
            }
        
        }


        public bool TimeOut()
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                return false;
            }
            return true;
        }
    
    
    
    
    }
}