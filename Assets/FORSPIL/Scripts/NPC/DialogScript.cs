﻿using System.Collections;
using System.Collections.Generic;
using DialogueEditor;
using UnityEngine;

namespace MyAssets.Scripts
{
    public class DialogScript : MonoBehaviour
    {
        public NPCConversation conversation;
        public Transform center;
        public float detectionArea;
        public LayerMask playerMask;
        public Transform player;

        public delegate void StatedConversation(string name);

        public static event StatedConversation dialog;

        public delegate void FinishedConversation();

        public static event FinishedConversation finisheDialog;

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;

            if (DetectPlayer())
            {
                Gizmos.color = Color.red;
            }

            Gizmos.DrawWireSphere(center.position, detectionArea);
        }

        private bool DetectPlayer()
        {
            Collider[] hitColliders = Physics.OverlapSphere(center.position, detectionArea, playerMask);

            foreach (var collider in hitColliders)
            {
                player = collider.transform;
                return true;
            }

            player = null;
            return false;
        }

        // Update is called once per frame
        void Update()
        {
            bool detectPlayer = DetectPlayer();

            if (detectPlayer && Input.GetButtonDown("StartConversation"))
            {
                StartCoroutine(StartConversation(conversation));
            }
        }

        public IEnumerator StartConversation(NPCConversation conversation)
        {
            ConversationManager.Instance.StartConversation(conversation);
            yield return null;
        }

        public void StartConversationMethod(string name)
        {
            dialog?.Invoke(name);
        }
        public void FinishedConversationMethod()
        {
            StartCoroutine(Wait());
            finisheDialog?.Invoke();
        }

        IEnumerator Wait()
        {
            yield return new WaitForSeconds(3.5f);
        }
    }
}