﻿using UnityEngine;

namespace MyAssets.Scripts.stuff_i_do_not_get_yet
{
   
    public class Door : MonoBehaviour
    {
        public BoxCollider boxCollider;
        
        public void Interact()
        {
            boxCollider.enabled = false;
        }

        public void resumeInteractableState()
        {
            boxCollider.enabled = true;
        }
    }
}