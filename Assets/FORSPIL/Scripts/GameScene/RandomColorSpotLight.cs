﻿using UnityEngine;
using UnityEngine.Serialization;

namespace MyAssets.Scripts
{
    public class RandomColorSpotLight : MonoBehaviour
    {
        public Light spotLight;
        public Light pointLight;
        public Material materialEmissionColor;

        private GameManager gameManager;
        private Color[] _colors;
        private static readonly int EmissionColor = Shader.PropertyToID("_EmissionColor");


        // Start is called before the first frame update
        void Start()
        {
            gameManager = FindObjectOfType<GameManager>();
            _colors = gameManager.Colors;

            Color randomColor = _colors[Mathf.Abs(Random.Range(0, _colors.Length - 1))];
            spotLight.color = randomColor;
            pointLight.color = Color.Lerp(randomColor, Color.white, .2f);
            materialEmissionColor.EnableKeyword("_EMISSION");
            materialEmissionColor.SetColor(EmissionColor, randomColor);

        }

   
    }
}
