﻿using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;


namespace MyAssets.Scripts
{
     public class MovingLights : MonoBehaviour
     {
          [Range(0f, 5f)] public float lerpTime;
          private Quaternion _randomRotation;
          private float _t;

          private void Start()
          {
               _randomRotation = Random.rotation;
          }

          private void Update()
          {
               transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(_randomRotation.eulerAngles), lerpTime * Time.deltaTime);

               _t = Mathf.Lerp(_t, 1f, lerpTime * Time.deltaTime);
               if (_t>.9f)
               {
                    _t = 0f;
                    _randomRotation = Random.rotation;
               }
          }
     }
}
