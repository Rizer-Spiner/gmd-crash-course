﻿using System;
using System.Collections;
using System.Collections.Generic;
using MyAssets.Scripts;
using UnityEngine;

public class CameraSwitch : MonoBehaviour
{

    public Animator Animator;
   

    private void OnEnable()
    {
        DialogScript.dialog += SetCameraState;
        DialogScript.finisheDialog += ResumeFreeState;
    }

    private void OnDisable()
    {
        DialogScript.dialog -= SetCameraState;
        DialogScript.finisheDialog -= ResumeFreeState;
    }

    public void ResumeFreeState()
    {
        Animator.Play("Movement");
    }

    public void SetCameraState(string name)
    {
      
        Animator.Play(name);
    }
    
}
