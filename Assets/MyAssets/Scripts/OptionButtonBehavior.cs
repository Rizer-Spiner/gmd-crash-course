﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class OptionButtonBehavior : MonoBehaviour
{
    public Sprite onSelected;
    public Sprite offSelected;

    public Image image;
    // public Button button;

    private GameManager GameManager;
    // Start is called before the first frame update
    void Start()
    {
        image.sprite = onSelected;
        GameManager = FindObjectOfType<GameManager>();
    }

    public void OnMouseDown()
    {
        if (image.sprite == onSelected)
        {
            image.sprite = offSelected;
        }
        else
        {
            image.sprite = onSelected;
        }
        
    }
}
