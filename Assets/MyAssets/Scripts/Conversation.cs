﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DialogueEditor;

public class Conversation : MonoBehaviour
{
    public NPCConversation MyConversation;
    public ConversationManager ConversationManager;

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ConversationManager.StartConversation(MyConversation);
        }
    }
}
